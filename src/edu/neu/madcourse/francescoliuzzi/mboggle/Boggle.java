/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband3 for more book information.
 ***/
package edu.neu.madcourse.francescoliuzzi.mboggle;


import edu.neu.madcourse.francescoliuzzi.R;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;


public class Boggle extends Activity implements OnClickListener {
	public static final String TAG = "MBoggle";
	public static String username = "mboggle_liuzzi";
	public static String password = "mboggle";

	public static final int MINUTE_INTERVAL = 10;

	private SharedPreferences sharedPrefs;
    
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mboggle_main);


		//set up polling service
		Log.d(TAG, "Setting up polling service...");
	
        String alarm = Context.ALARM_SERVICE;
        AlarmManager am = ( AlarmManager ) getSystemService( alarm );
         
        Intent intent = new Intent( "REFRESH_THIS" );
        PendingIntent pi = PendingIntent.getBroadcast( this, 0, intent, 0 );
         
        int type = AlarmManager.ELAPSED_REALTIME_WAKEUP;
        long interval = AlarmManager.INTERVAL_FIFTEEN_MINUTES;
        long triggerTime = SystemClock.elapsedRealtime() + interval;
         
        am.setInexactRepeating( type, triggerTime, interval, pi );
		
		
		// Set up click listeners for all the buttons
		View continueButton = findViewById(R.id.continue_button);
		continueButton.setOnClickListener(this);
		
		View newButton = findViewById(R.id.new_button);
		newButton.setOnClickListener(this);
		View aboutButton = findViewById(R.id.about_button);
		aboutButton.setOnClickListener(this);
		View exitButton = findViewById(R.id.exit_button);
		exitButton.setOnClickListener(this);

		View topScoresButton = findViewById(R.id.topscores_button);
		topScoresButton.setOnClickListener(this);
		View multiplayerButton = findViewById(R.id.multiplayer_button);
		multiplayerButton.setOnClickListener(this);
		
		
		this.sharedPrefs = 
				getApplicationContext().getSharedPreferences(BoggleGame.class.getSimpleName(), 
				Activity.MODE_PRIVATE);
        
        //if there is a saved game state...
        if(sharedPrefs.getBoolean("continue",false)){
        	continueButton.setVisibility(View.VISIBLE);
        }

	}

	@Override
	protected void onResume() {
		super.onResume();
		
		onCreate(null);
	}

	@Override
	protected void onPause() {
		super.onPause();
		BoggleMusic.stop(this);
	}

	public void onClick(View v) {
		Intent i;
		Log.d(TAG,"clicked on main");
		switch (v.getId()) {
		
		case R.id.continue_button:
			startGame(BoggleGame.CONTINUE);
			break;
			
		case R.id.multiplayer_button:
			i = new Intent(this, Multiplayer.class);
			startActivity(i);
			break;	

		case R.id.topscores_button:
			i = new Intent(this, BoggleTopScores.class);
			startActivity(i);
			break;
			// ...
		case R.id.about_button:
			i = new Intent(this, BoggleAbout.class);
			startActivity(i);
			break;
			// More buttons go here (if any) ...

		case R.id.new_button:
			Log.d(TAG,"pressed new game but");
			startGame(BoggleGame.NEWGAME);
			break;
		case R.id.exit_button:
			finish();
			break;
		}
	}


	/** Start a new game with the given difficulty level */
	private void startGame(int i) {
		Log.d(TAG, "clicked on " + i);
		Intent intent = new Intent(this, BoggleGame.class);
		intent.putExtra(BoggleGame.OPTION, i);
		startActivity(intent);
	}

	
}