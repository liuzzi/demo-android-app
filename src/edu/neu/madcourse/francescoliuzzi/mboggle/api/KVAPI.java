package edu.neu.madcourse.francescoliuzzi.mboggle.api;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import android.util.Log;
import edu.neu.madcourse.francescoliuzzi.mboggle.Boggle;
import edu.neu.madcourse.francescoliuzzi.mboggle.api.GetThread;
import edu.neu.madcourse.francescoliuzzi.mboggle.api.PutThread;

public class KVAPI {
		
	public static boolean put(String key, String val)
	{
		PutThread task = new PutThread(key,val);
		task.execute();
		try {
			task.get(12000, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			Log.e(Boggle.TAG,e.getMessage());
		} catch (ExecutionException e) {
			Log.e(Boggle.TAG,e.getMessage());
		} catch (TimeoutException e) {
			Log.e(Boggle.TAG,e.getMessage());
		}
		Log.d(Boggle.TAG,"returning from put -- value:"+task.result);
		return task.result;
	}

	public static String get(String key)
	{
		GetThread task = new GetThread(key);
		task.execute();
		try {
			task.get(12000, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			Log.e(Boggle.TAG,e.getMessage());
			return null;
		} catch (ExecutionException e) {
			Log.e(Boggle.TAG,e.getMessage());
			return null;
		} catch (TimeoutException e) {
			Log.e(Boggle.TAG,e.getMessage());
			return null;
		}
		return task.result;
	}
	
	public static boolean isServerAvailable()
	{
		CheckServerThread task = new CheckServerThread();
		task.execute();
		try {
			task.get(12000, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			Log.e(Boggle.TAG,e.getMessage());
		} catch (ExecutionException e) {
			Log.e(Boggle.TAG,e.getMessage());
		} catch (TimeoutException e) {
			Log.e(Boggle.TAG,e.getMessage());
		}
		Log.d(Boggle.TAG,"returning from put -- value:"+task.result);
		if (task.result == null) return false;
		
		return task.result;
	}
	
	
}
