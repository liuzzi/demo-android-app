package edu.neu.madcourse.francescoliuzzi.mboggle.api;

import android.os.AsyncTask;
import edu.neu.mobileclass.apis.KeyValueAPI;

public class CheckServerThread extends AsyncTask<Void, Integer, Void>{
	public Boolean result;

	public CheckServerThread()
	{
		result = null;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected Void doInBackground(Void... params) {
		result = KeyValueAPI.isServerAvailable();
		return null;
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
	}
}