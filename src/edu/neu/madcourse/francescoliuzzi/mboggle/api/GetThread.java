package edu.neu.madcourse.francescoliuzzi.mboggle.api;

import android.os.AsyncTask;
import android.util.Log;
import edu.neu.madcourse.francescoliuzzi.mboggle.Boggle;
import edu.neu.mobileclass.apis.KeyValueAPI;

public class GetThread extends AsyncTask<Void, Integer, Void>{
	private static final String TAG = "MBoggle";
	String key;
	String result;
	public boolean finished;

	public GetThread(String key)
	{
		this.key=key;
		this.result = null;
		finished = false;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected Void doInBackground(Void... params) {
		try{
			if (KeyValueAPI.isServerAvailable()){
				Log.d(TAG,"Server is available.");
				this.result = KeyValueAPI.get(Boggle.username, Boggle.password, key).trim();
				if(result.contains("ERROR"))
					result = null;
			}
		}catch(Exception e){
			Log.d(TAG,"error getting: "+e.getMessage());
		}
		return null;
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		finished = true;

	}
}