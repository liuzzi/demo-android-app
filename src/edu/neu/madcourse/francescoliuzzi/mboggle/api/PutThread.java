package edu.neu.madcourse.francescoliuzzi.mboggle.api;

import android.os.AsyncTask;
import android.util.Log;
import edu.neu.madcourse.francescoliuzzi.mboggle.Boggle;
import edu.neu.mobileclass.apis.KeyValueAPI;

public class PutThread extends AsyncTask<Void, Integer, Void>{
	private static final String TAG = "MBoggle";
	String key;
	String val;
	Boolean result;
	public boolean finished;
	public PutThread(String key, String val)
	{
		this.key=key;
		this.val=val;
		this.result = Boolean.valueOf(false);
		finished = false;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected Void doInBackground(Void... params) {
		try{
			if (KeyValueAPI.isServerAvailable()){
				Log.d(TAG,"Server is available.");
				KeyValueAPI.put(Boggle.username, Boggle.password, key, val);
				this.result = Boolean.valueOf(true);
			}
			else
				this.result = Boolean.valueOf(false);
		}catch(Exception e){
			Log.d(TAG,"error putting: "+e.getMessage());
		}
		return null;
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		finished = true;

	}
}