package edu.neu.madcourse.francescoliuzzi.mboggle;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;

import edu.neu.madcourse.francescoliuzzi.R;
import edu.neu.madcourse.francescoliuzzi.mboggle.api.KVAPI;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.util.Log;

public class PollingService extends IntentService {

	public final static int NOTIFICATION_ID = 145558;
	public HashSet<String> game_ids_already_notified;

	public PollingService(){
		super("Multiplayer Boggle");

		game_ids_already_notified = new HashSet<String>();
	}

	private void addNotifiedGameID(String gameid)
	{
		try{
			File file = new File(Environment.getExternalStorageDirectory() + File.separator + "notified_ids.txt");

			if(!file.exists())
				file.createNewFile();

			FileWriter fw = new FileWriter(file,true);
			BufferedWriter writer = new BufferedWriter(fw);
			writer.write(gameid);
			writer.newLine();
			writer.close();
		}
		catch(IOException e)
		{
			Log.e(Boggle.TAG,"ERROR WRITING NOTIFIED GAMES");
		}

	}

	private HashSet<String> getNotifiedGameIDs()
	{
		HashSet<String> hs = new HashSet<String>();

		try{
			File file = new File(Environment.getExternalStorageDirectory() + File.separator + "notified_ids.txt");

			if(!file.exists())
				return hs;

			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			String s = null;
			while ((s = br.readLine()) != null) {
				hs.add(s);
			}
		}
		catch(IOException e)
		{
			Log.e(Boggle.TAG,"ERROR READING NOTIFIED GAMES");
		}
		
		return hs;
	}


	@Override
	protected void onHandleIntent(Intent workIntent) {
		game_ids_already_notified = getNotifiedGameIDs();
		
		Log.d(Boggle.TAG,"SERVICE RUNNING!");
		// poll server 

		//get telephone num
		String my_num = Multiplayer.fixContactNumber(
				((TelephonyManager) getSystemService(TELEPHONY_SERVICE)).getLine1Number().trim());



		//Get all the games for this number
		String query = "user:"+my_num+":games";
		Log.d(Boggle.TAG,"querying:"+query);
		String result = KVAPI.get("user:"+my_num+":games");

		String line = null;

		if(result != null && !result.equals("")){
			Log.d(Boggle.TAG,"got back gamelist:"+result);
			String split[] = result.split(",");

			//for every game in the gamelist
			for(int i = split.length-1;i>=0;i--)
			{
				String game_id = split[i];
				if(game_id.length() < 2) continue;
				query = "games:"+game_id+":players";


				//get my score
				query = "games:"+game_id+":"+my_num+":score";
				int my_score = -1;
				result = KVAPI.get(query);
				if(result != null && !result.equals(""))
					my_score = Integer.parseInt(result);
				if(my_score == -1)
				{
					//check if game is async
					String mode = KVAPI.get("games:"+game_id+":mode");
					if(mode.equals("async")){
						//Make a notification in the task bar.

						if(!game_ids_already_notified.contains(game_id)){
							createNotification();
							game_ids_already_notified.add(game_id);
							addNotifiedGameID(game_id);
							Log.d(Boggle.TAG,"PENDING CHALLENGE DETECTED BY SERVICE..gameid:"+game_id);
						}
						else{
							Log.d(Boggle.TAG,"PENDING CHALLENGE Already notified user tho:"+game_id);
						}
					}
				}



			}
		}

		//now schedule another poll
		//scheduleNextUpdate();
	}


	private void createNotification() {

		NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		Intent nintent = new Intent();
		nintent.setClass(this, Multiplayer.class);
		PendingIntent pin = PendingIntent.getActivity(getApplicationContext(),
				0, nintent, 0);
		String title = "Multiplayer Boggle";
		String body = "New challenge from a friend!";
		Notification n = new Notification(R.drawable.ic_launcher, body,
				System.currentTimeMillis());
		n.contentIntent = pin;
		n.setLatestEventInfo(getApplicationContext(), title, body, pin);

		n.defaults = Notification.DEFAULT_ALL;
		nm.notify(NOTIFICATION_ID, n);
	}



}

