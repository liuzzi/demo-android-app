package edu.neu.madcourse.francescoliuzzi.mboggle;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import edu.neu.madcourse.francescoliuzzi.R;
import edu.neu.madcourse.francescoliuzzi.mboggle.api.KVAPI;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

public class BoggleTopScores extends Activity{
	private static final String TAG = "MBoggle";
	private String username = "anon";
	private int user_score;
	private ArrayList<String> list;
	private boolean made_top;
	private int rank;
	private boolean error_in_network;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mboggle_top_scores);


		error_in_network = false;
		list = new ArrayList<String>();
		made_top = false;
		rank = -1;

		Bundle b = getIntent().getExtras();
		int score = -1;
		Log.d(TAG,"Starting top scores intent");
		JSONArray jarray;
		if (b != null){
			score = getIntent().getExtras().getInt("score",-1);
			user_score = score;
			Log.d(TAG,"Receieved score: "+score);

			if(score >= 0 && savedInstanceState == null)
			{
				register_top_score(score);
			}
		}

		if(!made_top && score >= 0 && savedInstanceState == null && !error_in_network)
		{
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle("Sorry!");
			alertDialog.setMessage("You didn't make the high score list this time.");
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					// TODO Add your code for the button here.
				}
			});
			alertDialog.show();
		}

		String result = null;

		try{
			result = KVAPI.get("topscores");
		}catch(Exception e)
		{
			//here, result will be null
			result = null;
			Log.e(TAG,"error on get of high scores.");
		}

		if (result != null && !result.contains("ERROR")){
			try {
				jarray = (JSONArray) new JSONParser().parse(result);
				list = new ArrayList<String>();
				for(int i = 0; i < 10; i++)
				{
					list.add((String)jarray.get(i));
				}
			} catch (ParseException e) {
				Log.e(TAG,"ERROR parsing json from server");
			}
		}

		String output = "";
		if(result != null)
		{
			for(int i = 0; i < list.size(); i++)
			{
				String s = list.get(i);
				String ranking[] = s.split(" ");
				int user_name_len = ranking[0].length();
				int extra_spaces_needed = 10-user_name_len;
				
				String extra_spaces = "    ";
				for(int j = 0; j < extra_spaces_needed;j++)
				{
					extra_spaces += " ";
				}
				
				
				output += (i+1) + "\t\t\t" + ranking[2] + "\n\t\t\t\t\t\t" + ranking[0] + 
						"\n\t\t\t\t\t\t" + ranking[1] + "\n\n";  
			}
		}
		else
			output = "***   No Internet Connection!   ***";
		
		TextView tv = (TextView)findViewById(R.id.topscores_rankings);
		tv.setText(output);
	}



	// returns true if user score made the top score list
	private boolean register_top_score(int score)
	{
		Log.d(TAG,"checking against leaderboard...");
		
		//debug code to create a blank topscore json
		//
		//				JSONArray blank = new JSONArray();
		//				for(int i = 0; i < 10; i++)
		//				{
		//					blank.add("anon 00/00/00 0");
		//				}
		//		
		//				put("topscores",blank.toString());
		//		
		//				Log.d(TAG,"posted blank...");

		//get top scores json
		String result = null;
		try{
			result = KVAPI.get("topscores");
		}catch(Exception e){
			Log.d(TAG, "error retrieving:"+e.getMessage());
			result = null;
		}
		
		if (result != null && !result.contains("ERROR")){
			Log.d(TAG,"got top scorers list: "+result);
			JSONArray jarray;
			try {
				jarray = (JSONArray) new JSONParser().parse(result);
			} catch (ParseException e) {
				Log.e(TAG,"ERROR parsing json from server");
				return false;
			}

			list = new ArrayList<String>();

			for(int i = 0; i < jarray.size();i++)
			{
				String a_top_score[] = ((String) jarray.get(i)).split(" ");
				if(score > Integer.parseInt(a_top_score[2]) && !made_top)
				{
					made_top = true;
					rank = i;
					Log.d(TAG,"Made high score list.");
					AlertDialog.Builder alert = new AlertDialog.Builder(this);

					alert.setTitle("You made a high score!");
					alert.setMessage("Enter your name:");

					// Set an EditText view to get user input 
					final EditText input = new EditText(this);
					alert.setView(input);

					alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							Editable value = input.getText();
							username = value.toString();
							username = username.replaceAll("\\s", "_");
							if(username.length() > 10)
								username = username.substring(0,15);
							
							if(username.equals(""))
								username = "Anonymous";
								

							Log.d(TAG,"username inputted as:"+username);

							Date date = new Date(); // your date
							Calendar cal = Calendar.getInstance();
							cal.setTime(date);
							int year = cal.get(Calendar.YEAR);
							int month = cal.get(Calendar.MONTH)+1;
							int day = cal.get(Calendar.DAY_OF_MONTH);
							
							String last_two_digs_of_year = Integer.toString(year).substring(2);
							
							String to_add = username + " "+month+
									"/"+day+"/"+last_two_digs_of_year+" " + user_score;
							Log.d(TAG,"adding string to list: "+ to_add);
							list.add(rank,to_add);

							//bump off last person
							list.remove(list.size()-1);

							JSONArray new_top_jarray = new JSONArray();
							for(String s : list){
								new_top_jarray.add(s);
							}

							Log.d(TAG, "putting new:  " + list.toString());
							KVAPI.put("topscores", new_top_jarray.toString());
							//add rest
							onCreate(new Bundle());

						}
					});

					alert.show();



				}

				list.add((String)jarray.get(i));
			}
		}

		return made_top;
	}



	




}
