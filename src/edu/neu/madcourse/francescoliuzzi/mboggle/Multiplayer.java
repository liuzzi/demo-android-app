package edu.neu.madcourse.francescoliuzzi.mboggle;

import java.util.ArrayList;
import java.util.Collections;

import edu.neu.madcourse.francescoliuzzi.R;
import edu.neu.madcourse.francescoliuzzi.mboggle.api.KVAPI;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.ContactsContract.PhoneLookup;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Multiplayer extends Activity {

	private ArrayList<String> games;
	private ArrayList<String> challenges;
	ListView challengeList;
	ListView gamesList;

	private boolean showing_dialog = false;
	private boolean entered_game;
	private int retry_limit = 7;

	private String my_num = null;

	private ArrayList<String> opponants;

	private void alertNoConnection()
	{
		Toast.makeText(this, "Error: No Internet Connection!", Toast.LENGTH_LONG).show(); 
	}

	Button but;

	public void refreshList(View view)
	{
		onCreate(null);
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mboggle_multiplayer);
		syncing = false;
		entered_game = false;
		opponants = new ArrayList<String>();

		my_num = Multiplayer.fixContactNumber(
				((TelephonyManager) getSystemService(TELEPHONY_SERVICE)).getLine1Number().trim());
		Log.d(Boggle.TAG,"-----------phone taken as: " + my_num);
		//this.my_num =  my_num.substring(1);


		//set up list view of games
		games = new ArrayList<String>();
		challenges = new ArrayList<String>();
		challengeList = (ListView)findViewById(R.id.challengelistview);
		gamesList = (ListView)findViewById(R.id.gamelistview);
		gamesList.setSelector(new ColorDrawable(0));


		final ProgressDialog dialog = ProgressDialog.show(Multiplayer.this, "","Signing in..." , true);
		dialog.show();
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			public void run() {
				createGameListView();
				Log.d(Boggle.TAG,"Signing in handler.  ");
				createChallengeListView();
				dialog.dismiss();
			}   
		},750);


		// this opens the activity. note the  Intent.ACTION_GET_CONTENT
		// and the intent.setType
		((Button)findViewById(R.id.pick_person)).setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!KVAPI.isServerAvailable()){
					alertNoConnection();
					return;
				}
				// user BoD suggests using Intent.ACTION_PICK instead of .ACTION_GET_CONTENT to avoid the chooser
				Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
				// BoD con't: CONTENT_TYPE instead of CONTENT_ITEM_TYPE
				intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
				startActivityForResult(intent, 1);                
			}
		});

		but = ((Button)findViewById(R.id.pick_person));



		if(!KVAPI.isServerAvailable()){
			alertNoConnection();
			return;
		}
		
		
		NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
	    nm.cancel(PollingService.NOTIFICATION_ID);
	}

	public ArrayList<String> getReversed(ArrayList<String> original)
	{
		ArrayList<String> copy = new ArrayList<String>(original);
		Collections.reverse(copy);
		return copy;
	}

	private void createChallengeListView()
	{
		//Create an adapter for the listView and add the ArrayList to the adapter.
		challengeList.setAdapter(new ArrayAdapter<String>(Multiplayer.this, 
				android.R.layout.simple_list_item_1,challenges));
		challengeList.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3)
			{
				//args2 is the listViews Selected index
				setupAndJoinGame(arg2);
				entered_game = true;
			}
		});
	}

	private boolean syncing = false;

	private void setupAndJoinGame(int index)
	{		
		String challenge = opponants.get(index);
		if(challenge.equals("")){
			Toast.makeText(this, "Already Played!", Toast.LENGTH_LONG).show();
			return;
		}

		String opp_num = challenge.split(" ")[0];
		String game_id = challenge.split(" ")[1];
		String opp_score = challenge.split(" ")[2];
		String opp_name = challenge.split(" ")[3];

		Log.d(Boggle.TAG,"Setting up a join game against: " + opp_num);

		//put that we're in game now
		String query = "games:"+game_id+":"+my_num+":inGame";
		KVAPI.put(query,"true");
		Log.d(Boggle.TAG,"putting in-game: "+query);

		intent = new Intent(this, BoggleGame.class);
		intent.putExtra("my_num",my_num);
		intent.putExtra("opp_num",opp_num);

		//this means its a sync game
		if(opp_score.equals("-1"))
		{
			intent.putExtra("mode","sync");
		}
		else{
			intent.putExtra("async_response",true);
		}

		//opponent hasnt registered a score
		if(opp_score.equals(""))
		{
			intent.putExtra("opp_score",0);
		}
		else
			intent.putExtra("opp_score",Integer.parseInt(opp_score));


		intent.putExtra("opp_name",opp_name);

		//get board
		String result = KVAPI.get("games:"+game_id+":board");

		if( result != null && !result.equals(""))
		{
			Log.d(Boggle.TAG,"retrieved board: " + result);
			intent.putExtra("board", result);

			final ProgressDialog dialog = ProgressDialog.show(Multiplayer.this, 
					"","syncing.." , true);
			dialog.show();

			myHandler = new Handler();
			Runnable mUpdateTimeTask = new Runnable() {
				public void run() {
					if (!syncing){
						syncing = true;
						myHandler.postDelayed(this, 5000);
					}
					else
					{
						syncing = false;
						dialog.dismiss();
						startActivity(intent);
					}
				}
			};
			mUpdateTimeTask.run();


		}
	}

	private Intent intent = null;

	@Override
	protected void onResume() {
		super.onResume();
		if(entered_game){
			onCreate(null);
		}

	}

	private void createGameListView()
	{
		boolean found_sync_game = false;


		//Get all the games for this number
		String query = "user:"+this.my_num+":games";
		Log.d(Boggle.TAG,"querying:"+query);
		String result = KVAPI.get("user:"+this.my_num+":games");

		String line = null;

		if(result != null && !result.equals("")){
			Log.d(Boggle.TAG,"got back gamelist:"+result);
			String split[] = result.split(",");

			//for every game in the gamelist
			for(int i = split.length-1;i>=0;i--)
			{
				String game_id = split[i];
				if(game_id.length() < 2) continue;
				query = "games:"+game_id+":players";

				Log.d(Boggle.TAG,"querying: "+query);
				//Get the other player of the game
				result = KVAPI.get(query);
				Log.d(Boggle.TAG,"got: "+result);


				if(result != null && !result.equals("")){
					String[] results = result.split(",");
					String opp_num = results[1];
					if(opp_num.equals(this.my_num))
					{
						opp_num = results[0];
					}

					//get my score
					query = "games:"+game_id+":"+my_num+":score";
					int my_score = -1;
					result = KVAPI.get(query);
					if(result != null && !result.equals(""))
						my_score = Integer.parseInt(result);


					Log.d(Boggle.TAG,"querying:"+query);
					Log.d(Boggle.TAG,"my score from queried game:"+my_score);


					int opp_score = -1;
					//get opp_score
					result = KVAPI.get("games:"+game_id+":"+opp_num+":score");
					if(result != null && !result.equals(""))
						opp_score = Integer.parseInt(result);

					String displayName = null;
					Uri phoneUri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(opp_num));
					Cursor phonesCursor = getApplicationContext().getContentResolver().query(phoneUri, new String[] {PhoneLookup.DISPLAY_NAME}, null, null, null);

					if(phonesCursor != null && phonesCursor.moveToFirst()) {
						displayName = phonesCursor.getString(0); // this is the contact name
					}//end if 
					line = displayName;
					String opp_name = displayName;

					//when number is unknown (not in contacts)
					if(line == null || line.equals("null\n"))
					{
						line = "+" + opp_num;
						opp_name = opp_num;
					}



					if(my_score != -1 && opp_score != -1){
						if(my_score > opp_score)
						{
							line += "\n\tWon.  \n\t" + my_score + " to "+ opp_score;
						}
						else if(my_score == opp_score){
							line += "\n\tTied.  \n\t" + my_score + " to "+ opp_score;
						}
						else{
							line += "\n\tLost.  \n\t" + my_score + " to "+ opp_score;
						}
					}
					else{

						if(opp_score == -1){
							line += "\n\tWaiting for opponent.";
						}
						else
						{
							line += "\n\tOpponent Score: "+opp_score;
						}
						if(my_score == -1){
							line += "\n\tUnplayed.";
						}
						else
						{
							line += "\n\tMy Score: "+my_score;
						}
					}


					//deal with the challenges
					String mode = KVAPI.get("games:"+game_id+":mode");
					if(mode.equals("async")){
						//if(challenges.size() == 5) break; //stop at most 5 challenges



						if(my_score == -1)
						{
							opponants.add(opp_num + " " + game_id+ " "+opp_score+" "+opp_name);
							Log.d(Boggle.TAG,"Noticed a challenge from+"+opp_num+"\n"+line);
							challenges.add(line);
							continue;
						}

					}
					else
					{
						found_sync_game = true;
						//if(challenges.size() == 5) break; //stop at most 5 challenges
						Log.d(Boggle.TAG,"sync challenge found!");

						line += "\n\t\tSynchronous Challenge!";


						if((my_score != -1 && opp_score == -1) ||
								(opp_score != -1 && my_score == -1)){
							line += "\n        Interrupted!";
						}
						else if(my_score == -1 && opp_score == -1)
						{
							opponants.add(opp_num + " " + game_id+ " "+opp_score+" "+opp_name);
							Log.d(Boggle.TAG,"Noticed a sync challenge from+"+opp_num+"\n"+line);
							challenges.add(line);
							continue;
						}
					}

					games.add(line);

				}
				else{
					Log.e(Boggle.TAG,"Error expanding games list");
					break;
				}
			}
		}


		//Create an adapter for the listView and add the ArrayList to the adapter.
		gamesList.setAdapter(new ArrayAdapter<String>(Multiplayer.this, 
				android.R.layout.simple_list_item_1,games));
		gamesList.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3)
			{
				//args2 is the listViews Selected index
			}
		});
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data != null) {
			Uri uri = data.getData();

			if (uri != null) {
				Cursor c = null;
				try {
					c = getContentResolver().query(uri, new String[]{ 
							ContactsContract.CommonDataKinds.Phone.NUMBER,  
							ContactsContract.Contacts.DISPLAY_NAME},
							null, null, null);

					if (c != null && c.moveToFirst()) {
						String number = c.getString(0);
						String name = c.getString(1);
						opp_num = number;
						opp_name = name;
						openNewGameDialogThenSetup();

					}
				} finally {
					if (c != null) {
						c.close();
					}
				}
			}
		}
	}

	public static String fixContactNumber(String contact_num)
	{
		//replace non numerics

		Log.d(Boggle.TAG,"before:"+contact_num);
		contact_num = contact_num.replaceAll( "\\D+", "" );
		contact_num = contact_num.substring(contact_num.length() - 10); 
		Log.d(Boggle.TAG,"after:"+contact_num);
		return contact_num;
	}

	/** Ask the user what difficulty level they want */
	private void openNewGameDialogThenSetup() {
		showing_dialog = true;

		new AlertDialog.Builder(this)
		.setTitle(R.string.new_game_title)
		.setItems(R.array.multiplayer_type,
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialoginterface,
					int i) {
				entered_game = true;
				showSelectedNumber(opp_name, opp_num);
				startGeneration(i);
			}
		})
		.show();
	}


	protected String opp_num = null;
	protected String opp_name = null;
	protected int game_id;
	protected int opp_score;
	protected String board = null;

	public void startGeneration(int i)
	{
		my_num = fixContactNumber(my_num);
		opp_num = fixContactNumber(opp_num);
		boolean sync = false;

		board = BoggleGame.generateBoard(25);


		//put in server invite
		//		games:#id:board gameBoard
		//		games:#id:mode async or sync
		//		games:#id:players = phone1, phone2
		//		games:#id:phone1:inGame boolean
		//		games:#id:phone2:inGame boolean
		//		games:#id:phone1:score number    NOT STO
		//		games:#id:phone2:score number     NOT STOR
		//		games:#id:startTime                    NOT STORED NOW
		//		user:#id:games listof games:#id

		game_id = board.hashCode();


		//BOARD
		String key = "games:"+game_id+":board";
		String val = board;
		KVAPI.put(key, val);
		Log.d(Boggle.TAG,"made call:"+key+"\t"+val);

		//MODE
		key = "games:"+game_id+":mode";
		if( i == 0)
			val = "async";
		else{
			val = "sync";
			sync = true;
		}

		Log.d(Boggle.TAG,"mode selected as "+val);

		KVAPI.put(key, val);
		Log.d(Boggle.TAG,"made call:"+key+"\t"+val);

		//PLAYERS
		key = "games:"+game_id+":players";
		val = my_num + "," + opp_num;
		KVAPI.put(key, val);
		Log.d(Boggle.TAG,"made call:"+key+"\t"+val);

		//PHONE1 INGAME
		key = "games:"+game_id+":"+my_num+":inGame";
		val = "true";
		KVAPI.put(key, val);
		Log.d(Boggle.TAG,"made call:"+key+"\t"+val);

		//PHONE2 INGAME
		key = "games:"+game_id+":"+opp_num+":inGame";
		val = "false";
		KVAPI.put(key, val);
		Log.d(Boggle.TAG,"made call:"+key+"\t"+val);

		appendGameList(game_id,my_num);
		appendGameList(game_id,opp_num);

		Log.d(Boggle.TAG,"Finished making calls. Launching game.");

		//wait for player2 to join
		if(sync){
			//set in game and waiting
			//KVAPI.put("games:"+game_id+":"+my_num+":inGame", "true");

			final ProgressDialog dialog = ProgressDialog.show(Multiplayer.this, 
					"","Waiting 20s for other player...\n\nHave them enter " +
							"multiplayer/refresh to accept their challenge" , true);
			dialog.show();
			myHandler = new Handler();
			Runnable mUpdateTimeTask = new Runnable() {
				public void run() {
					boolean p2_joined = hasP2Joined(opp_num);

					if(retry_limit == 0)
					{
						dialog.cancel();
						displayCouldntFindP2();
						retry_limit = 7;
					}
					else if (!p2_joined){
						myHandler.postDelayed(this, 3000 );
						Log.d(Boggle.TAG,"ERROR: Couldnt find other player! waiting another 5 secs");
						retry_limit--;
					}
					else
					{
						Log.d(Boggle.TAG,"Player 2 joined.  Starting game.");
						//starts game with extra options
						dialog.dismiss();
						startSyncGame();
					}

				}
			};
			mUpdateTimeTask.run();
		}
		else{

			//starts game with extra options
			Intent intent = new Intent(this, BoggleGame.class);
			intent.putExtra("my_num",my_num);
			intent.putExtra("opp_name", opp_name);
			intent.putExtra("opp_num",opp_num);
			intent.putExtra("board", board);
			intent.putExtra("board_id", game_id);

			intent.putExtra("mode", "async");

			startActivity(intent);
		}
	}

	private void displayCouldntFindP2()
	{
		Toast.makeText(this, "ERROR: Couldnt find other player!", Toast.LENGTH_LONG).show();
		removeFromGameList(my_num);
		removeFromGameList(opp_num);
	}

	private void startSyncGame()
	{
		//starts game with extra options
		Intent intent = new Intent(this, BoggleGame.class);
		intent.putExtra("my_num",my_num);
		intent.putExtra("opp_name", opp_name);
		intent.putExtra("opp_num",opp_num);
		intent.putExtra("board", board);
		intent.putExtra("board_id", game_id);

		intent.putExtra("mode", "sync");

		startActivity(intent);
	}

	Handler myHandler = null;

	//returns true if p2 joined.
	private boolean hasP2Joined(String opp_num)
	{
		//PHONE2 INGAME
		String key = "games:"+game_id+":"+opp_num+":inGame";
		String result = KVAPI.get(key);
		if(result != null && result.equals("true")){
			Log.d(Boggle.TAG,"Asking server if p2 is in game yet:" + opp_num + " : "+result);
			return true;
		}
		else
			return false;	
	}

	private void appendGameList(int game_id, String my_num)
	{
		//user:#id:games listof games:#id
		String result = KVAPI.get("user:"+my_num+":games");
		if(result.equals("") || result == null)
		{
			//store first game value
			result = Integer.toString(game_id);
			Log.d(Boggle.TAG,"append game list: starting first game val:"+result);
		}
		else
		{
			//append
			result += ","+game_id;

			String split[] = result.split(",");
			String new_gen = "";

			for(int i = 0; i < split.length;i++)
			{
				if(i == 10) break;
				String s = split[split.length - (i+1)];
				if(s.length() > 1)
					new_gen = ","+s + new_gen;

			}

			result = new_gen.substring(1);

			Log.d(Boggle.TAG,"append game list to already existing: "+result);
		}
		KVAPI.put("user:"+my_num+":games",result);
	}

	private static String arrayListToCSV(ArrayList<String> list)
	{
		String result = "";
		for(String s : list)
		{
			result+=","+s;
		}
		return result.substring(1);
	}


	private static ArrayList<String> getReversed(String[] array)
	{
		ArrayList<String> result = new ArrayList<String>();
		for(int i = array.length-1; i >=0; i--)
		{
			if( i < 0) break;
			result.add(array[i]);

		}
		return result;
	}



	private void removeFromGameList(String num)
	{
		//user:#id:games listof games:#id
		String result = KVAPI.get("user:"+num+":games");
		if(result != null && !result.equals(""))
		{
			String split[] = result.split(",");

			Log.d(Boggle.TAG,"deleting game from gamelist...before: "+result);

			if(split.length > 1)
				result = result.replaceAll(","+game_id, "");

			Log.d(Boggle.TAG,"after: "+result);
		}

		KVAPI.put("user:"+num+":games",result);
	}

	public void showSelectedNumber(String name, String number) {
		Toast.makeText(this, "Invite sent to " + name, Toast.LENGTH_LONG).show();      
	}

}
