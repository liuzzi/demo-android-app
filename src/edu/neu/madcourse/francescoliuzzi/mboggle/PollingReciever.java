package edu.neu.madcourse.francescoliuzzi.mboggle;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class PollingReciever extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(Boggle.TAG,"POLLING RECEIVER HIT!");
		Intent myIntent = new Intent(context, PollingService.class);
		myIntent.setAction("edu.neu.madcourse.francescoliuzzi.mboggle.PollingService");
		context.startService(myIntent);
	}
}
