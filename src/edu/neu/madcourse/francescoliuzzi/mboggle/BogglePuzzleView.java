
package edu.neu.madcourse.francescoliuzzi.mboggle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;

import edu.neu.madcourse.francescoliuzzi.R;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.graphics.Shader;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Parcelable;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;


public class BogglePuzzleView extends View {

	private static final String TAG = "Boggle";

	private static final String SELX = "selX"; 
	private static final String SELY = "selY";
	private static final String VIEW_STATE = "viewState";
	private static final int ID = 42; 


	private float width;    // width of one tile
	private float height;   // height of one tile
	private int selX;       // X index of selection
	private int selY;       // Y index of selection

	private String clock;

	private ArrayList<Tile> selectionList;

	private boolean touch = false;
	private boolean madeValidWord=false;
	private boolean paused = false;
	private boolean out_of_time = false;
	private boolean red_flash = false;



	public CountDownTimer clock_timer;
	public CountDownTimer valid_flash_timer;

	protected long millisLeft;

	private final BoggleGame game;

	public BogglePuzzleView(Context context, long millisOnTimer) {
		super(context);
		this.game = (BoggleGame) context;
		setFocusable(true);
		setFocusableInTouchMode(true);
		this.game.chosenWords = new HashSet<String>();
		selectionList = new ArrayList<Tile>();
		this.game.score = 0;
		if(millisOnTimer < 0) millisOnTimer = 120000;
		else this.millisLeft = millisOnTimer;



		clock_timer = new CountDownTimer(millisOnTimer,1000) {

			@Override
			public void onTick(long millisUntilFinished) {
				clock = formatIntoHHMMSS(millisUntilFinished/1000);
				if(!paused)
					millisLeft = millisUntilFinished;
				else
					this.cancel();

				invalidate();
			}

			@Override
			public void onFinish() {
				clock = "OUT OF TIME!";
				out_of_time = true;
				invalidate();
				game.endGame();
			}
		};
		clock = "";
		clock_timer.start();

		setId(ID); 
	}


	static String formatIntoHHMMSS(long secsIn)
	{
		long remainder = secsIn % 3600;
		long minutes = remainder / 60;
		long seconds = remainder % 60;

		return (  (minutes < 10 ? "0" : "") + minutes
				+ ":" + (seconds< 10 ? "0" : "") + seconds );
	}


	@Override
	protected Parcelable onSaveInstanceState() { 
		Parcelable p = super.onSaveInstanceState();
		Bundle bundle = new Bundle();
		bundle.putInt(SELX, selX);
		bundle.putInt(SELY, selY);
		bundle.putParcelable(VIEW_STATE, p);
		return bundle;
	}
	@Override
	protected void onRestoreInstanceState(Parcelable state) { 
		Bundle bundle = (Bundle) state;
		select(bundle.getInt(SELX), bundle.getInt(SELY));
		super.onRestoreInstanceState(bundle.getParcelable(VIEW_STATE));
	}


	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		width = w / 9f;
		height = h / 9f;

		super.onSizeChanged(w, h, oldw, oldh);
	}

	@Override
	protected void onDraw(Canvas canvas) {

		int screen_height = getHeight();
		int screen_width = getWidth();
		int half_height = screen_height/2;
		int half_width = screen_width/2;
		int padding = screen_width/20;
		int tile_width = screen_width/5;
		int tile_height = tile_width;

		this.height = tile_height;
		this.width = tile_width;


		// Draw the background...
		Paint background = new Paint();


		if(madeValidWord && !red_flash)
			background.setColor(getResources().getColor(
					R.color.boggle_puzzle_background_valid));
		else if(red_flash){
			background.setColor(getResources().getColor(
					R.color.boggle_puzzle_hilite));

		}
		else if(paused){
			background.setColor(getResources().getColor(
					R.color.boggle_puzzle_paused));
			background.setShader(new LinearGradient(0, 0, 0, getHeight(), 
					getResources().getColor(
							R.color.boggle_puzzle_paused), 
							getResources().getColor(
									R.color.transparent_black), 
									Shader.TileMode.MIRROR));
		}
		else if(!out_of_time){
			background.setColor(getResources().getColor(
					R.color.boggle_puzzle_background));
			background.setShader(new LinearGradient(0, 0, 0, getHeight(), 
					getResources().getColor(
							R.color.boggle_puzzle_background), 
							getResources().getColor(
									R.color.transparent_black), 
									Shader.TileMode.MIRROR));
		}


		canvas.drawRect(0, 0, getWidth(), getHeight()/2+tile_height, background);

		//draw transparent black overlay
		if(madeValidWord)
			background.setColor(getResources().getColor(
					R.color.boggle_puzzle_background_valid));
		else if(red_flash){
			background.setColor(getResources().getColor(
					R.color.boggle_puzzle_hilite));
		}
		else{
			background.setColor(getResources().getColor(
					R.color.transparent_black));
		}

		canvas.drawRect(0, getHeight()/2+tile_height, getWidth(), getHeight(), background);


		Paint foreground = new Paint(Paint.ANTI_ALIAS_FLAG);

		foreground.setColor(getResources().getColor(
				R.color.boggle_puzzle_foreground));


		//Score text
		foreground.setTextSize(tile_height * 0.35f);
		canvas.drawText("Score:"+this.game.score, padding, 
				screen_height-padding*5, foreground);

		foreground.setTextSize(tile_height * 0.45f);
		if(paused){
			canvas.drawText(formatIntoHHMMSS(millisLeft/1000), padding, 
					screen_height-padding, foreground);

			foreground.setTextSize(tile_height * 0.55f);
			canvas.drawText("Tap to Resume", padding, screen_height/2, foreground);
			return;
		}

		if(!out_of_time)
			canvas.drawText(clock, padding, 
					screen_height-padding, foreground);

		Paint pausedText = new Paint();
		pausedText.setTextSize(tile_height * 0.45f);
		pausedText.setColor(getResources().getColor(
				R.color.boggle_puzzle_hilite));

		if(!out_of_time && !this.game.synchronous)
			canvas.drawText("PAUSE", tile_width*2, 
					screen_height+(1/16*screen_height)-padding*2, pausedText);


		if(this.game.synchronous){
			//TODO: display opponent score or something
		}

		// Define colors for the grid lines
		Paint hilite = new Paint();
		hilite.setStyle(Paint.Style.STROKE); 
		hilite.setStrokeWidth(10);
		hilite.setColor(getResources().getColor(R.color.boggle_puzzle_hilite));


		// Draw the letters...

		foreground.setStyle(Style.FILL);
		foreground.setTextScaleX(tile_width / height);
		foreground.setTextSize(tile_height * 0.85f);
		foreground.setTextAlign(Paint.Align.CENTER);
		foreground.setColor(getResources().getColor(
				R.color.boggle_puzzle_foreground));

		Paint outer_circles = new Paint();
		outer_circles.setColor(getResources().getColor(R.color.boggle_puzzle_circles));
		outer_circles.setStyle(Paint.Style.FILL);
		
		Paint inner_selected_circles = new Paint();
		inner_selected_circles.setColor(getResources().getColor(R.color.boggle_puzzle_circles));
		inner_selected_circles.setStyle(Paint.Style.FILL);
		
		Paint inner_circles = new Paint();
		inner_circles.setColor(getResources().getColor(R.color.boggle_puzzle_circles));
		inner_circles.setStyle(Paint.Style.FILL);

		Paint selectedCircles = new Paint();
		selectedCircles.setColor(getResources().getColor(R.color.boggle_puzzle_selected_circles));
		selectedCircles.setStyle(Paint.Style.FILL);

		// Draw the number in the center of the tile
		FontMetrics fm = foreground.getFontMetrics();
		// Centering in X: use alignment (and X at midpoint)
		float x = tile_width / 2;
		// Centering in Y: measure ascent/descent first
		float y = tile_height / 2 - (fm.ascent + fm.descent) / 2;
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				String tileString = this.game.getTileString(i, j);
				Tile t = new Tile();
				t.x = i;
				t.y = j;
				t.character = tileString;

				if(selectionList.contains(t)){
					canvas.drawCircle(i
							* tile_width + x, j * tile_height + y - padding - padding/3, 70, selectedCircles);
					foreground.setTextSize(tile_height * 0.95f);
					
					canvas.drawCircle(i
							* tile_width + x, j * tile_height + y - padding - padding/3, 50, inner_selected_circles);
					foreground.setTextSize(tile_height * 0.95f);
					canvas.drawText(tileString, i
							* tile_width + x, j * tile_height + y, foreground);
				}
				else{
					canvas.drawCircle(i
							* tile_width + x, j * tile_height + y - padding - padding/3, 55, inner_circles);
					foreground.setTextSize(tile_height * 0.85f);
					
					canvas.drawCircle(i
							* tile_width + x, j * tile_height + y - padding - padding/3, 50, inner_circles);
					foreground.setTextSize(tile_height * 0.95f);
					canvas.drawText(tileString, i
							* tile_width + x, j * tile_height + y, foreground);
				}
			}
		}

		if(out_of_time)
		{
			int text_spacing = padding;
			int count = 1;
			int column = 1;

			//			foreground.setTextSize(tile_height * 0.35f);
			//			canvas.drawText("Words Found:", tile_width*2, 
			//					padding*2, foreground);
			//			foreground.setTextSize(tile_height * 0.20f);
			//			Iterator<String> it = this.game.chosenWords.iterator();
			//			while(it.hasNext()){
			//				if(count % 10 == 0){
			//					column++;
			//					canvas.drawText(it.next(), tile_width*column, 
			//							text_spacing*(count+7), foreground);
			//				}
			//				else
			//					canvas.drawText(it.next(), tile_width*3, 
			//							text_spacing*(count+7), foreground);
			//				count++;
			//				
			//			}
			//			
			background.setColor(getResources().getColor(
					R.color.boggle_puzzle_gameover));
			canvas.drawRect(0, 0, getWidth(), getHeight(), background);
			foreground.setTextSize(tile_height * 0.40f);
			canvas.drawText("GAME OVER!", tile_width*2, 
					half_height+tile_height+tile_height/2, foreground);

			return;
		}

		//Word text
		foreground.setTextSize(tile_height * 0.90f);
		foreground.setColor(getResources().getColor(R.color.boggle_puzzle_foreground));
		canvas.drawText(selectionToString(), tile_width + x*2, 
				screen_height/2+tile_height+padding*2+tile_height, foreground);



	}

	private String selectionToString()
	{
		String result = "";
		for(Tile t : selectionList){
			result += t.character;
		}
		return result;
	}


	private void scoreWord(String word)
	{
		this.game.score += 1000*((word.length()-3)*3+1);
	}

	private void setFlashTimer()
	{
		if(valid_flash_timer != null) valid_flash_timer.cancel();

		valid_flash_timer = new CountDownTimer(100,1000) {
			@Override
			public void onTick(long millisUntilFinished) {

			}

			@Override
			public void onFinish() {
				madeValidWord = false;
				red_flash = false;
				invalidate();
			}
		};
		valid_flash_timer.start();

	}

	private void checkWord()
	{
		String word = selectionToString().toLowerCase();
		if(!this.game.chosenWords.contains(word) && isValidWord(word)){
			this.game.chosenWords.add(word);
			scoreWord(word);
			madeValidWord = true;

			//flash screen
			setFlashTimer();
			invalidate();

			// Is the sound loaded already?
			if (this.game.loaded_sound) {
				this.game.soundPool.play(this.game.valid_word_sound_id, 1, 1, 1, 0, 1f);
				Log.d(TAG, "valid word + sound");
				this.game.v.vibrate(100);
			}


			Log.d("boggle","valid word: "+word);
		}
		else if(this.game.chosenWords.contains(word) && word.length() > 2){

			red_flash = true;
			setFlashTimer();

		}
		else if (selectionList.size() > 2 && this.game.loaded_sound) {
				this.game.soundPool.play(this.game.invalid_word_sound_id, 1, 1, 1, 0, 1f);
				Log.d("boggle","invalid word + sound: "+word);
				red_flash = true;
				setFlashTimer();
		}


	}

	private boolean isValidWord(String word)
	{
		int length = word.length();
		if(length < 3) return false;

		String firstTwoLetters = word.substring(0,2).toLowerCase();
		InputStream is;
		try {
			is = game.getAssets().open("dict/"+firstTwoLetters+"_"+length);

			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line;
			while ((line = reader.readLine()) != null) {
				if(line.equals(word)) return true;
			}      
			reader.close();

		} catch (IOException e) {
			Log.e("boggle", e.getMessage());
		}

		return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {

		if(!this.game.synchronous && event.getY() > getHeight()/2 + (getWidth()/5)*2){
			pauseGame();
			return true;
		}

		if(out_of_time) return true;

		if(paused && !touch){
			resumeGame(millisLeft);
			return true;
		}

		if (event.getAction() == MotionEvent.ACTION_UP){
			checkWord();

			selectionList.clear();
			touch = false;
			invalidate();
			return super.onTouchEvent(event);
		}

		touch = true;

		int touchx = (int) (event.getX() / width);
		int touchy = (int) (event.getY() / height);

		int safety = getWidth()/25;
		int checkXLeft = (int) ((event.getX()+safety) / width);
		int checkYLeft = (int) ((event.getY()+safety) / height);	
		int checkXRight = (int) ((event.getX()-safety) / width);
		int checkYRight = (int) ((event.getY()-safety) / height);	

		if(checkXLeft != touchx ||
				checkYLeft != touchy ||
				checkXRight != touchx ||
				checkYRight != touchy)
			return true;



		select(touchx,touchy);


		return true;
	}


	private boolean isSelectionValid(Tile t)
	{
		if(selectionList.contains(t) || !isAdjacentTile(t)) return false;

		return true;
	}

	private boolean isAdjacentTile(Tile current)
	{
		if(selectionList.size() == 0) return true;

		Tile previous = selectionList.get(selectionList.size()-1);
		if(!(Math.abs(current.x - previous.x) > 1
				|| Math.abs(current.y - previous.y) > 1))
			return true;
		return false;
	}

	public void pauseGame()
	{
		clock_timer.cancel();
		paused = true;
		invalidate();
	}

	public void resumeGame(long millisleft)
	{
		Log.d(TAG,"Resuming game: millisLeft:"+millisLeft);
		clock_timer = new CountDownTimer(millisLeft,1000) {

			@Override
			public void onTick(long millisUntilFinished) {
				clock = formatIntoHHMMSS(millisUntilFinished/1000);
				if(!paused)
					millisLeft = millisUntilFinished;
				else
					cancel();
				invalidate();
			}

			@Override
			public void onFinish() {
				clock = "OUT OF TIME!";
				out_of_time = true;
				game.endGame();
				invalidate();
			}
		};
		clock_timer.start();
		paused = false;
		invalidate();
	}


	private void select(int x, int y) {
		if(y > 4){
			return;
		}

		Tile t = new Tile();
		t.x=x;
		t.y=y;
		t.character=game.getTileString(x, y);


		if(isSelectionValid(t)){
			this.game.v.vibrate(30);
			selectionList.add(t);
			if (this.game.loaded_sound) {
				this.game.soundPool.play(this.game.letter_pop_id, 1, 1, 1, 0, 1f);
			}
		}
		invalidate();

//		selX = Math.min(Math.max(x, 0), 8);
//		selY = Math.min(Math.max(y, 0), 8);
//		invalidate();
	}

	private void getRect(int x, int y, Rect rect) {
		rect.set((int) (x * width), (int) (y * height), (int) (x
				* width + width), (int) (y * height + height));
	}

	//container for storing tile sequences.
	private class Tile
	{
		String character;
		int x;
		int y;
		@Override
		public boolean equals(Object o) {
			if (o instanceof Tile)
				return x == ((Tile)o).x
				&& y == ((Tile)o).y; 
			else
				return false;
		}

		@Override
		public int hashCode() {
			// TODO Auto-generated method stub
			return x*y+character.hashCode();
		}
	}

	// ...
}

