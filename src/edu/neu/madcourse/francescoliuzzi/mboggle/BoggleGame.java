package edu.neu.madcourse.francescoliuzzi.mboggle;

import java.util.HashSet;
import java.util.Random;

import edu.neu.madcourse.francescoliuzzi.R;
import edu.neu.madcourse.francescoliuzzi.mboggle.api.KVAPI;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

public class BoggleGame extends Activity {
	private static final String TAG = "Boggle";

	public static final String OPTION =
			"org.example.sudoku.difficulty";
	private static final String PREF_PUZZLE = "puzzle" ;
	private static final String PREF_TIME = "time_remaining" ;
	private static final String PREF_WORDS = "chosen_words" ;
	private static final String PREF_SCORE = "score" ;
	private static final String PREF_GAMEOVR = "game_over" ;

	public static final int NEWGAME = 0;
	public static final int MULTIPLAYER = 2;
	protected static final int CONTINUE = 1;

	private String puzzle[];

	public SoundPool soundPool;
	public int valid_word_sound_id;
	public int invalid_word_sound_id;
	public int letter_pop_id;

	public boolean game_over;

	protected HashSet<String> chosenWords;
	protected int score;
	private Handler myHandler;

	public boolean loaded_sound = false;

	private String board ="STNGEIAEDRLSSEPO";
	private int sync_await_retry_limit = 5;

	private BogglePuzzleView puzzleView;

	protected boolean multiplayer;

	public Vibrator v;

	private static boolean isInLastFive(String is_in, String looking_for)
	{
		if (is_in.length() < 5)
			return false;

		Log.d(TAG, "length greater than 5" + is_in.substring(is_in.length()-5).contains(looking_for));

		return is_in.substring(is_in.length()-5).contains(looking_for);
	}

	private static String genLetter(int roll, Random rand, String all)
	{
		String str = "";

		if(roll < 19 && roll > 3)
		{
			str+="E";
			if (isInLastFive(all,str) == true){
				Log.d(TAG, str + " : Calling gen letter again");
				str = genLetter(20, rand, all);
			}
			else
				Log.d(TAG, str);
		}
		else if(roll < 30)
		{
			str+="T";
			if (isInLastFive(all,str)){
				str =	genLetter(31, rand, all);
			}
			else
				Log.d(TAG, str);
		}
		else if(roll < 44)
		{
			if(rand.nextInt(2) == 0)
				str+="A";
			else
				str+="R";
			if (isInLastFive(all,str)){
				str =	genLetter(45, rand, all);
			}
			else
				Log.d(TAG, str);
		}
		else if(roll < 55)
		{	
			String poss = "INO";
			str += String.valueOf(poss.charAt(rand.nextInt(poss.length())));

			if (isInLastFive(all,str)){
				str =	genLetter(56, rand, all);
			}
			else
				Log.d(TAG, str);
		}
		else if(roll < 64)
		{
			str+="S";

			if (isInLastFive(all,str)){
				str =	genLetter(65, rand, all);
			}
			else
				Log.d(TAG, str);
		}
		else if(roll < 70)
		{
			str+="D";

			if (isInLastFive(all,str)){
				str =	genLetter(71, rand, all);
			}
			else
				Log.d(TAG, str);
		}
		else if(roll < 75)
		{
			String poss = "CHL";
			str += String.valueOf(poss.charAt(rand.nextInt(poss.length())));

			//			if (isInLastFive(all,str)){
			//				genLetter(76, rand, all);
			//			}
		}
		else if(roll < 79)
		{
			String poss = "FMPU";
			str += String.valueOf(poss.charAt(rand.nextInt(poss.length())));
		}
		else
		{
			String poss = "BJKGY";
			str += String.valueOf(poss.charAt(rand.nextInt(poss.length())));
		}

		Log.d(TAG, str + "     ::    whole:   "+all);

		return str;
	}

	/**
	 * generates a board given the number of letters
	 * -- uses boggle dice probabilities
	 */
	public static String generateBoard(int num_letters)
	{
		String str = "";

		for(int i = 0; i < num_letters;i++){
			Random rand = new Random();
			int roll = rand.nextInt(85)+1;
			str += genLetter(roll, rand, str);
		}
		return str;
	}

	private void showFinalResultsDialog()
	{
		boolean won = false;
		if(score > opp_score)
			won = true;

		String line;
		if(won){
			line = "You beat " + opp_name + "!";
			line += "\n"+score+" to " +opp_score;
		}
		else{
			line = "You lost to " + opp_name + "!";
			line += "\n"+score+" to " +opp_score;
		}

		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle("Results:");
		alertDialog.setMessage(line);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				toTopScores();
			}
		});
		alertDialog.show();
	}

	public void endGame()
	{
		prefsEditor.putBoolean("continue", false);
		prefsEditor.commit();

		Log.d(TAG,"Ending game...score:"+this.score);
		//		games:#id:phone1:score number
		//		games:#id:phone2:score number

		//set my score
		KVAPI.put("games:"+board_id+":"+my_num+":score", 
				Integer.toString(score));
		Log.d(Boggle.TAG,my_num+" - -"+board_id+"  setting score to: " + Integer.toString(score));

		//set out_of_game
		String key = "games:"+board_id+":"+my_num+":inGame";
		String val = "false";
		KVAPI.put(key, val);

		if(getIntent().hasExtra("async_response"))
		{
			showFinalResultsDialog();
		}
		else if(getIntent().hasExtra("mode") && getIntent().getStringExtra("mode").equals("sync"))
		{

			final ProgressDialog dialog = ProgressDialog.show(BoggleGame.this, 
					"","Waiting for other player to finish..." , true);
			dialog.show();

			myHandler = new Handler();
			Runnable mUpdateTimeTask = new Runnable() {
				public void run() {

					String result = KVAPI.get("games:"+board_id+":"+opp_num+":score");

					if(sync_await_retry_limit == 0)
					{
						dialog.dismiss();
						displayCouldntFindP2();
						sync_await_retry_limit = 5;
						toTopScores();
					}
					else if (result == null || result.equals("") || Integer.parseInt(result) < 0){
						myHandler.postDelayed(this, 5000 );
						Log.d(Boggle.TAG,"ERROR: Couldnt find other player! waiting another 5 secs");
						sync_await_retry_limit--;
					}
					else
					{
						Log.d(Boggle.TAG,"Player 2 joined.  Starting game.");
						//starts game with extra options
						dialog.dismiss();

						opp_score = Integer.parseInt(result);

						showFinalResultsDialog();
					}

				}
			};
			mUpdateTimeTask.run();
		}
		else{
			toTopScores();
		}


	}

	private void displayCouldntFindP2()
	{
		Toast.makeText(this, "ERROR: Couldnt find other player! Refresh later", Toast.LENGTH_LONG).show();
	}

	private boolean isPlayerOutOfGame(String opp_num)
	{
		//set out_of_game
		String result = KVAPI.get("games:"+board_id+":"+opp_num+":inGame");
		return (result != null && result.equals("true"));
	}

	private void toTopScores()
	{
		Intent i = new Intent(this, BoggleTopScores.class);
		i.putExtra("score", this.score);
		startActivity(i);
		finish();
	}

	private void setupSingleplayer()
	{
		multiplayer = false;
		board = BoggleGame.generateBoard(25);

		// ...
		// If the activity is restarted, do a continue next time
		getIntent().putExtra(OPTION, CONTINUE);

		prefsEditor.putBoolean("continue", true);
		prefsEditor.commit();
	}


	protected String my_num = null;
	protected String opp_num = null;
	protected String opp_name = null;
	protected String board_id = null;
	protected int opp_score;
	protected boolean synchronous = false;

	private void setupMultiplayer()
	{
		multiplayer = true;
		board = getIntent().getStringExtra("board");
		my_num = getIntent().getStringExtra("my_num");
		opp_score = getIntent().getIntExtra("opp_score",0);
		opp_num = getIntent().getStringExtra("opp_num");
		opp_name = getIntent().getStringExtra("opp_name");

		if (opp_name.trim().length() < 2)
			opp_name = opp_num;

		if(getIntent().hasExtra("mode"))
			if(getIntent().getStringExtra("mode").equals("sync")){
				synchronous = true;
				Log.d(Boggle.TAG,"setting sync flag to true in BoggleGame");
			}

		board_id = Integer.toString(board.hashCode());
		Log.d(TAG, "multiplayer board: "+board+"\nopp info:"+opp_name+"  "+opp_num);


		//wait for player 2 if its sync

		//set start time -- epoch
	}


	private SharedPreferences sharedPrefs;
	private Editor prefsEditor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.sharedPrefs = getApplicationContext().getSharedPreferences(BoggleGame.class.getSimpleName(), 
				Activity.MODE_PRIVATE);
		this.prefsEditor = sharedPrefs.edit();

		//keep the screen on
		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		Log.d(TAG, "onCreate");
		game_over = false;

		// Get instance of Vibrator from current Context
		this.v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);

		//Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		//Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		//grab whether or not a new game or continue before setting up..
		int diff = getIntent().getIntExtra(OPTION,NEWGAME);

		if( getIntent().hasExtra("my_num") )
		{
			//this is a multiplayer game
			Log.d(TAG,"Starting multiplayer BoggleGame");
			setupMultiplayer();
		}
		else{
			//this is a singleplayer game
			Log.d(TAG,"Starting singleplayer BoggleGame");
			setupSingleplayer();
		}

		//continue game setup...
		puzzle = getPuzzle(diff);
		setContentView(puzzleView);
		puzzleView.requestFocus();

		// Set the hardware buttons to control the music
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		// Load the sound
		soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);

		valid_word_sound_id = soundPool.load(this, R.raw.valid_word, 1);
		invalid_word_sound_id = soundPool.load(this, R.raw.invalid_word, 1);
		letter_pop_id = soundPool.load(this, R.raw.letter_pop, 1);
		loaded_sound = true;
	}

	private HashSet<String> getHashSetFromStringSet(String[] arr){
		HashSet<String> set = new HashSet<String>();
		for(String s : arr)
		{
			set.add(s);
		}
		return set;
	}

	@Override
	protected void onResume() {
		super.onResume();

		if(!this.synchronous)
			BoggleMusic.play(this, R.raw.nero_electron);
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "onPause");
		BoggleMusic.stop(this);
		//		// Save the current puzzle
		//		puzzleView.clock_timer.cancel();
		if(!game_over)
			this.puzzleView.pauseGame();

		getPreferences(MODE_PRIVATE).edit().putString(PREF_PUZZLE,
				toPuzzleString(puzzle)).commit();
		getPreferences(MODE_PRIVATE).edit().putLong(PREF_TIME,
				puzzleView.millisLeft).commit();
		getPreferences(MODE_PRIVATE).edit().putString("chosen_words",
				chosenWordsToString(chosenWords)).commit();
		getPreferences(MODE_PRIVATE).edit().putInt(PREF_SCORE,
				score).commit();
		getPreferences(MODE_PRIVATE).edit().putBoolean(PREF_GAMEOVR,
				game_over).commit();
	}
	
	private String chosenWordsToString(HashSet<String> hs)
	{
		String result = "";
		for (String s : hs)
		{
			result += s+",";
		}
		if(!result.equals(""))
			return result.substring(0,result.length()-1);
		else
			return "";
	}
	
	private HashSet<String> chosenWordsFromString(String s)
	{
		HashSet<String> hs = new HashSet<String>();
		if(s.equals(""))
			return hs;
		
		String[] split = s.split(",");
		for(String word : split)
		{
			if (!word.equals(""))
			{
				hs.add(word);
			}
		}
		return hs;
	}

	/** Given a difficulty level, come up with a new puzzle */
	private String[] getPuzzle(int diff) {
		String puz;

		if(diff == CONTINUE){

			puzzleView = new BogglePuzzleView(this,getPreferences(MODE_PRIVATE).getLong(PREF_TIME,0));
			puz = getPreferences(MODE_PRIVATE).getString(PREF_PUZZLE,"");
			score = getPreferences(MODE_PRIVATE).getInt(PREF_SCORE,0);
			chosenWords = chosenWordsFromString(getPreferences(MODE_PRIVATE).getString(PREF_WORDS,",,"));			
			game_over = getPreferences(MODE_PRIVATE).getBoolean(PREF_GAMEOVR,false);
			this.puzzleView.millisLeft = getPreferences(MODE_PRIVATE).getLong(PREF_TIME,0);
		}
		else{
			puz = board;
			puzzleView = new BogglePuzzleView(this,-1);
		}

		return fromPuzzleString(puz);
	}

	/** Convert an array into a puzzle string */
	static private String toPuzzleString(String[] puz) {
		StringBuilder buf = new StringBuilder();
		for (String element : puz) {
			buf.append(element);
		}
		return buf.toString();
	}

	/** Convert a puzzle string into an array */
	static protected String[] fromPuzzleString(String string) {
		String[] puz = new String[string.length()];
		for (int i = 0; i < puz.length; i++) {
			puz[i] = String.valueOf(string.charAt(i));
		}
		return puz;
	}

	/** Return the tile at the given coordinates */
	private String getTile(int x, int y) {
		return puzzle[y * 5 + x];
	}


	/** Return a string for the tile at the given coordinates */
	protected String getTileString(int x, int y) {
		return getTile(x, y);
	}



}
