package edu.neu.madcourse.francescoliuzzi;


import edu.neu.madcourse.francescoliuzzi.sudoku.Sudoku;

import edu.neu.mobileClass.PhoneCheckAPI;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PhoneCheckAPI.doAuthorization(this);

        setTitle("Francesco Liuzzi");
    }

    public void createError(View view) {
    	throw new RuntimeException();
    }
    
    public void StartTeamMembers(View view) {
    	Intent intent = new Intent(this, TeamMembers.class);
    	startActivity(intent);
    }
    
    public void startSudoku(View view) {
    	Intent intent = new Intent(this, Sudoku.class);
    	startActivity(intent);
    }
    
    public void startBoggle(View view) {
    	Intent intent = new Intent(this, edu.neu.madcourse.francescoliuzzi.boggle.Boggle.class);
    	startActivity(intent);
    }
    
    public void startMultiplayerBoggle(View view) {
    	Intent intent = new Intent(this, edu.neu.madcourse.francescoliuzzi.mboggle.Boggle.class);
    	startActivity(intent);
    }
    
}
