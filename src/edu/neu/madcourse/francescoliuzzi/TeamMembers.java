package edu.neu.madcourse.francescoliuzzi;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.widget.TextView;

public class TeamMembers extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_team_members);
        setTitle("Team Members");
        
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        
        TextView imei = (TextView) findViewById(R.id.imei);
        imei.setText(telephonyManager.getDeviceId());
	}


}
