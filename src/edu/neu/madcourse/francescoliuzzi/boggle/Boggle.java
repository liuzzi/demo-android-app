/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband3 for more book information.
 ***/
package edu.neu.madcourse.francescoliuzzi.boggle;


import edu.neu.madcourse.francescoliuzzi.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;


public class Boggle extends Activity implements OnClickListener {
	private static final String TAG = "Boggle";

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.boggle_main);

		// Set up click listeners for all the buttons
		View continueButton = findViewById(R.id.continue_button);
		continueButton.setOnClickListener(this);
		View newButton = findViewById(R.id.new_button);
		newButton.setOnClickListener(this);
		View aboutButton = findViewById(R.id.about_button);
		aboutButton.setOnClickListener(this);
		View exitButton = findViewById(R.id.exit_button);
		exitButton.setOnClickListener(this);

	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		BoggleMusic.stop(this);
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.continue_button:
			startGame(BoggleGame.CONTINUE);
			break;
			// ...
		case R.id.about_button:
			Intent i = new Intent(this, BoggleAbout.class);
			startActivity(i);
			break;
			// More buttons go here (if any) ...
		case R.id.new_button:
			startGame(BoggleGame.NEWGAME);
			break;
		case R.id.exit_button:
			finish();
			break;
		}
	}


	/** Start a new game with the given difficulty level */
	private void startGame(int i) {
		Log.d(TAG, "clicked on " + i);
		Intent intent = new Intent(this, BoggleGame.class);
		intent.putExtra(BoggleGame.OPTION, i);
		startActivity(intent);
	}
}