package edu.neu.madcourse.francescoliuzzi.boggle;

import java.util.HashSet;
import java.util.Random;
import java.util.Stack;

import edu.neu.madcourse.francescoliuzzi.R;
import android.app.Activity;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

public class BoggleGame extends Activity {
	private static final String TAG = "Boggle";

	public static final String OPTION =
			"org.example.sudoku.difficulty";
	private static final String PREF_PUZZLE = "puzzle" ;
	private static final String PREF_TIME = "time_remaining" ;
	private static final String PREF_WORDS = "chosen_words" ;
	private static final String PREF_SCORE = "score" ;

	public static final int NEWGAME = 0;
	protected static final int CONTINUE = 1;

	private String puzzle[];

	public SoundPool soundPool;
	public int valid_word_sound_id;
	public int invalid_word_sound_id;
	public int letter_pop_id;

	protected HashSet<String> chosenWords;
	protected int score;

	public boolean loaded_sound = false;

	private String board ="STNGEIAEDRLSSEPO";

	private BogglePuzzleView puzzleView;

	private static boolean isInLastFive(String is_in, String looking_for)
	{
		if (is_in.length() < 5)
			return false;

		Log.d(TAG, "length greater than 5" + is_in.substring(is_in.length()-5).contains(looking_for));

		return is_in.substring(is_in.length()-5).contains(looking_for);
	}

	private static String genLetter(int roll, Random rand, String all)
	{
		String str = "";

		if(roll < 19 && roll > 3)
		{
			str+="E";
			if (isInLastFive(all,str) == true){
				Log.d(TAG, str + " : Calling gen letter again");
				str = genLetter(20, rand, all);
			}
			else
				Log.d(TAG, str);
		}
		else if(roll < 30)
		{
			str+="T";
			if (isInLastFive(all,str)){
				str =	genLetter(31, rand, all);
			}
			else
				Log.d(TAG, str);
		}
		else if(roll < 44)
		{
			if(rand.nextInt(2) == 0)
				str+="A";
			else
				str+="R";
			if (isInLastFive(all,str)){
				str =	genLetter(45, rand, all);
			}
			else
				Log.d(TAG, str);
		}
		else if(roll < 55)
		{	
			String poss = "INO";
			str += String.valueOf(poss.charAt(rand.nextInt(poss.length())));

			if (isInLastFive(all,str)){
				str =	genLetter(56, rand, all);
			}
			else
				Log.d(TAG, str);
		}
		else if(roll < 64)
		{
			str+="S";

			if (isInLastFive(all,str)){
				str =	genLetter(65, rand, all);
			}
			else
				Log.d(TAG, str);
		}
		else if(roll < 70)
		{
			str+="D";

			if (isInLastFive(all,str)){
				str =	genLetter(71, rand, all);
			}
			else
				Log.d(TAG, str);
		}
		else if(roll < 75)
		{
			String poss = "CHL";
			str += String.valueOf(poss.charAt(rand.nextInt(poss.length())));

			//			if (isInLastFive(all,str)){
			//				genLetter(76, rand, all);
			//			}
		}
		else if(roll < 79)
		{
			String poss = "FMPU";
			str += String.valueOf(poss.charAt(rand.nextInt(poss.length())));
		}
		else
		{
			String poss = "BJKGY";
			str += String.valueOf(poss.charAt(rand.nextInt(poss.length())));
		}

		Log.d(TAG, str + "     ::    whole:   "+all);

		return str;
	}

	private String generateBoard()
	{
		String str = "";

		for(int i = 0; i < 16;i++){
			Random rand = new Random();
			int roll = rand.nextInt(85)+1;
			str += genLetter(roll, rand, str);
		}
		return str;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate");

		board = generateBoard();

		//Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		//Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		int diff = getIntent().getIntExtra(OPTION,NEWGAME);

		puzzle = getPuzzle(diff);


		setContentView(puzzleView);
		puzzleView.requestFocus();

		// ...
		// If the activity is restarted, do a continue next time
		getIntent().putExtra(OPTION, CONTINUE);



		// Set the hardware buttons to control the music
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		// Load the sound
		soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);

		valid_word_sound_id = soundPool.load(this, R.raw.valid_word, 1);
		invalid_word_sound_id = soundPool.load(this, R.raw.invalid_word, 1);
		letter_pop_id = soundPool.load(this, R.raw.letter_pop, 1);
		loaded_sound = true;

	}

	@Override
	protected void onResume() {
		super.onResume();
		BoggleMusic.play(this, R.raw.nero_electron);
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "onPause");
		BoggleMusic.stop(this);
		// Save the current puzzle

		getPreferences(MODE_PRIVATE).edit().putString(PREF_PUZZLE,
				toPuzzleString(puzzle)).commit();
		getPreferences(MODE_PRIVATE).edit().putLong("time_remaining",
				puzzleView.millisLeft).commit();
		getPreferences(MODE_PRIVATE).edit().putString("chosen_words",
				chosenWordsToString(chosenWords)).commit();
		getPreferences(MODE_PRIVATE).edit().putInt("score",
				score).commit();
	}

	
	private String chosenWordsToString(HashSet<String> hs)
	{
		String result = "";
		for (String s : hs)
		{
			result += s+",";
		}
		if(!result.equals(""))
			return result.substring(0,result.length()-1);
		else
			return "";
	}
	
	private HashSet<String> chosenWordsFromString(String s)
	{
		HashSet<String> hs = new HashSet<String>();
		if(s.equals(""))
			return hs;
		
		String[] split = s.split(",");
		for(String word : split)
		{
			if (!word.equals(""))
			{
				hs.add(word);
			}
		}
		return hs;
	}

	/** Given a difficulty level, come up with a new puzzle */
	private String[] getPuzzle(int diff) {
		String puz;

		if(diff == CONTINUE){
			puzzleView = new BogglePuzzleView(this,getPreferences(MODE_PRIVATE).getLong(PREF_TIME,0));
			puz = getPreferences(MODE_PRIVATE).getString(PREF_PUZZLE,"");
			score = getPreferences(MODE_PRIVATE).getInt(PREF_SCORE,0);
			chosenWords = chosenWordsFromString(getPreferences(MODE_PRIVATE).getString(PREF_WORDS,",,"));
		}
		else{
			puz = board;
			puzzleView = new BogglePuzzleView(this,-1);
		}

		return fromPuzzleString(puz);
	}

	/** Convert an array into a puzzle string */
	static private String toPuzzleString(String[] puz) {
		StringBuilder buf = new StringBuilder();
		for (String element : puz) {
			buf.append(element);
		}
		return buf.toString();
	}

	/** Convert a puzzle string into an array */
	static protected String[] fromPuzzleString(String string) {
		String[] puz = new String[string.length()];
		for (int i = 0; i < puz.length; i++) {
			puz[i] = String.valueOf(string.charAt(i));
		}
		return puz;
	}

	/** Return the tile at the given coordinates */
	private String getTile(int x, int y) {
		return puzzle[y * 4 + x];
	}


	/** Return a string for the tile at the given coordinates */
	protected String getTileString(int x, int y) {
		return getTile(x, y);
	}

}
