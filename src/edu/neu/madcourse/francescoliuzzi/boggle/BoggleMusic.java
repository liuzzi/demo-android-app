/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband3 for more book information.
 ***/
package edu.neu.madcourse.francescoliuzzi.boggle;

import android.content.Context;
import android.media.MediaPlayer;

public class BoggleMusic {
	private static MediaPlayer music = null;
	private static MediaPlayer sound_effects;

	/** Stop old song and start new one */

	public static void play(Context context, int resource) {
		stop(context);


		// Start music only if not disabled in preferences
		if (BogglePrefs.getMusic(context)) {
			music = MediaPlayer.create(context, resource);
			music.setLooping(true);
			music.start();
		}
	}

	public static void play_sound_effect(Context context, int resource) {
		stop(context);
		
		if (BogglePrefs.getMusic(context)) {
			if(sound_effects == null)
				sound_effects = MediaPlayer.create(context, resource);
			
			sound_effects.setLooping(false);
			sound_effects.start();
		}
	}


	/** Stop the music */
	public static void stop(Context context) { 
		if (music != null) {
			music.stop();
			music.release();
			music = null;
		}
	}
}
